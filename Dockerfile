FROM python:3.6-alpine
ENV POSTGRESQL_URL=postgresql://worker:worker@postgres/app
WORKDIR /code
RUN apk add --no-cache gcc musl-dev linux-headers libpq-dev
COPY ./requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
ENTRYPOINT ./entrypoint.sh

